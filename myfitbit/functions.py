from dietdaniel.settings import *


def calculate_maintenance_calories(weight):
    maintenance = int(round(weight * 17, 2))

    return maintenance


def calculate_daily_calories(weight):
    maintenance = calculate_maintenance_calories(weight)
    daily_calories = int(round(maintenance + CALORIES_SURPLUS_VERY_ACTIVE + CALORIES_SURPLUS_GAIN))

    return daily_calories


def calculate_daily_carbs(calories, protein, fat):
    total_carbs_calories = int(round(calories - (protein['calories'] + fat['calories']), 2))
    total_carbs_grams = int(round(total_carbs_calories / CALORIES_PER_GRAM_CARBS, 2))

    return {'calories': total_carbs_calories, 'grams': total_carbs_grams}


def calculate_daily_protein(weight):
    total_protein_grams = int(round(weight * 1.5, 2))
    total_protein_calories = int(round(total_protein_grams * CALORIES_PER_GRAM_PROTEIN, 2))

    return {'calories': total_protein_calories, 'grams': total_protein_grams}


def calculate_daily_fat(calories):
    total_fat_calories = int(round(calories * 0.25, 2))
    total_fat_grams = int(round(total_fat_calories / CALORIES_PER_GRAM_FAT, 2))

    return {'calories': total_fat_calories, 'grams': total_fat_grams}


def percent_in_range(nutrient, percent):
    """
    See if the percentage of the nutrient passed in is within
    a given satisfactory range of total calorie percentage.
    """
    in_range = 1 if \
        (MY_CALORIES_PERCENT[nutrient] - MY_CALORIES_PERCENT['range']) <= percent \
        <= (MY_CALORIES_PERCENT[nutrient] + MY_CALORIES_PERCENT['range']) \
        else 0

    return in_range