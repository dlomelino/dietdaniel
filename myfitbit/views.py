from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime, timedelta
import math
import fitbit
from dietdaniel.settings import *
from functions import *

client = fitbit.Fitbit(CONSUMER_KEY, CONSUMER_SECRET, resource_owner_key=OAUTH_TOKEN, resource_owner_secret=OAUTH_SECRET)


def index(request):
    today = datetime.today().date()
    yesterday = (datetime.now() - timedelta(hours=24)).date()

    use_date = today

    body = get_body_details(use_date)
    maintenance = calculate_maintenance_calories(body['weight'])
    foods = get_foods_summary(use_date)
    water_goal = get_water_goal()

    targets = {}
    targets['calories'] = calculate_daily_calories(body['weight'])
    targets['protein'] = calculate_daily_protein(body['weight'])
    targets['fat'] = calculate_daily_fat(targets['calories'])
    targets['carbs'] = calculate_daily_carbs(targets['calories'], targets['protein'], targets['fat'])
    targets['sodium'] = RECOMMENDED_DAILY_SODIUM_MAX
    targets['fiber'] = RECOMMENDED_DAILY_FIBER
    targets['water'] = water_goal

    recommended = {
        'carbs': {
            'percent': MY_CALORIES_PERCENT['carbs'],
        },
        'protein': {
            'percent': MY_CALORIES_PERCENT['protein'],
        },
        'fat': {
            'percent': MY_CALORIES_PERCENT['fat'],
        },
        'sodium': {
            'min': RECOMMENDED_DAILY_SODIUM_MIN,
            'max': RECOMMENDED_DAILY_SODIUM_MAX,
        },
        'fiber': RECOMMENDED_DAILY_FIBER,
        'potassium': RECOMMENDED_DAILY_POTASSIUM,
    }

    percents = {
        'carbs': {
            'value': 0,
            'in_range': 0,
        },
        'protein': {
            'value': 0,
            'in_range': 0,
        },
        'fat': {
            'value': 0,
            'in_range': 0,
        },
        'sodium': 0,
        'fiber': 0,
        'water': 0
    }
    if foods['calories']:
        percents['carbs']['value'] = int(round((float(foods['carbs']['calories']) / float(foods['calories'])) * 100, 2))
        percents['carbs']['in_range'] = percent_in_range('carbs', percents['carbs']['value'])
        percents['protein']['value'] = int(round((float(foods['protein']['calories']) / float(foods['calories'])) * 100, 2))
        percents['protein']['in_range'] = percent_in_range('protein', percents['protein']['value'])
        percents['fat']['value'] = int(round((float(foods['fat']['calories']) / float(foods['calories'])) * 100, 2))
        percents['fat']['in_range'] = percent_in_range('fat', percents['fat']['value'])
        percents['sodium'] = int(round((float(foods['sodium']) / targets['sodium']) * 100, 2))
        percents['fiber'] = int(round((float(foods['fiber']) / targets['fiber']) * 100, 2))
    if foods['water']:
        percents['water'] = int(round((float(foods['water']) / targets['water']) * 100, 2))

    diffs = {
        'calories': {},
        'carbs': {
            'calories': {},
            'grams': {},
        },
        'protein': {
            'calories': {},
            'grams': {},
        },
        'fat': {
            'calories': {},
            'grams': {},
        },
    }
    diffs['calories']['under'] = targets['calories'] - foods['calories']
    diffs['calories']['over'] = foods['calories'] - targets['calories']
    diffs['carbs']['calories']['under'] = targets['carbs']['calories'] - foods['carbs']['calories']
    diffs['carbs']['calories']['over'] = foods['carbs']['calories'] - targets['carbs']['calories']
    diffs['carbs']['grams']['under'] = targets['carbs']['grams'] - foods['carbs']['grams']
    diffs['carbs']['grams']['over'] = foods['carbs']['grams'] - targets['carbs']['grams']
    diffs['protein']['calories']['under'] = targets['protein']['calories'] - foods['protein']['calories']
    diffs['protein']['calories']['over'] = foods['protein']['calories'] - targets['protein']['calories']
    diffs['protein']['grams']['under'] = targets['protein']['grams'] - foods['protein']['grams']
    diffs['protein']['grams']['over'] = foods['protein']['grams'] - targets['protein']['grams']
    diffs['fat']['calories']['under'] = targets['fat']['calories'] - foods['fat']['calories']
    diffs['fat']['calories']['over'] = foods['fat']['calories'] - targets['fat']['calories']
    diffs['fat']['grams']['under'] = targets['fat']['grams'] - foods['fat']['grams']
    diffs['fat']['grams']['over'] = foods['fat']['grams'] - targets['fat']['grams']

    context_dict = {
        'body': body,
        'maintenance': maintenance,
        'foods': foods,
        'targets': targets,
        'recommended': recommended,
        'percents': percents,
        'diffs': diffs,
    }

    return render(request, 'myfitbit/index.html', context_dict)


def get_body_details(date):
    bodyweight = client.get_bodyweight(base_date=date)

    return {
        'weight': bodyweight['weight'][0]['weight'],
        'fat': round(bodyweight['weight'][0]['fat'], 2),
        'bmi': bodyweight['weight'][0]['bmi'],
    }


def get_foods_summary(date):
    foods = client._COLLECTION_RESOURCE('foods/log', date=date)

    foods_summary = foods['summary']

    foods_summary = {
        'calories': foods_summary['calories'],
        'carbs': {
            'calories': int(round(foods_summary['carbs'], 2)) * CALORIES_PER_GRAM_CARBS,
            'grams': int(round(foods_summary['carbs'], 2)),
        },
        'protein': {
            'calories': int(round(foods_summary['protein'], 2)) * CALORIES_PER_GRAM_PROTEIN,
            'grams': int(round(foods_summary['protein'], 2)),
        },
        'fat': {
            'calories': int(round(foods_summary['fat'], 2)) * CALORIES_PER_GRAM_FAT,
            'grams': int(round(foods_summary['fat'], 2)),
        },
        'sodium': int(round(foods_summary['sodium'], 2)),
        'fiber': int(round(foods_summary['fiber'], 2)),
        'water': round(foods_summary['water'], 1),
    }

    return foods_summary


def get_water_goal():
    water_goal = math.ceil(client.water_goal()['goal']['goal'])

    return water_goal